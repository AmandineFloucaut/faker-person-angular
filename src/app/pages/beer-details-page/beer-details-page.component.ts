
import { BeerComponent } from '@/app/components/beer/beer.component';
import { Beer } from '@/app/models/beer.model';
import { BeerService } from '@/app/services/beer.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-beer-details-page',
  templateUrl: './beer-details-page.component.html',
  styleUrls: ['./beer-details-page.component.scss']
})
export class BeerDetailsPageComponent implements OnInit {

  private beerId?: number;
  @Input()
  public beer?: Beer;

  @Output('beerDetails')
  public beerDetailEvent = new EventEmitter<Beer>();
  @Output('deleted')
  public deletedEvent = new EventEmitter<Beer>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private beerService: BeerService,
    private router: Router

  ) {
    // On passe l'id récupéré dans l'url en type number
    this.beerId = parseInt(this.activatedRoute.snapshot.params['id'], 10);

    // b = beer
    const beer = this.beerService.beers.find((b) => b.id === this.beerId);

    if (!beer) {
      // IMPORTANT
      return;
      ;
    }

    this.beer = beer;


  }

  ngOnInit(): void {
  
  }

  /**
   * goToBeerDetail
   */
  public goToBeerDetail() {
    console.log("click beer detail");
    
    this.beerDetailEvent.emit(this.beer);
  }

  get volume(){
    if(this.beer){
      return this.beer.volValue + " " + this.beer.volUnit;
    } else {
      return "Cet utilisateur n'existe pas";
    }

  }

  get foodsPairing(){
    let foods = "";
    if(this.beer){
      for(let food of this.beer.foodPairing){
         foods += food + ",  ";
      }
    }

   return foods;
  }

  // STEP 2 Delete Beer in array method witch send an event
  public clickOnDeletedBeer(){
    console.log("deleted");
    console.log(this.beer);
    this.deletedEvent.emit(this.beer);
  }

}
