import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { BeerComponent } from '@/components/beer/beer.component';
import { Beer } from '@/models/beer.model';
import { BeerService } from '@/services/beer.service';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public presentation: string = "Cet annuaire permet d'ajouter des gens au hasard grâce à une API super fun !";

  constructor(public beerService: BeerService){}

  ngOnInit(): void {
  }

}
