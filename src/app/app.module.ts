import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LucideAngularModule, Home, UserPlus, Lightbulb, Gift,  Trash2} from 'lucide-angular';
import { BeerComponent } from '@/components/beer/beer.component';
import { HomePageComponent } from '@/pages/home-page/home-page.component';
import { BeerDetailsPageComponent } from '@/pages/beer-details-page/beer-details-page.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    BeerComponent,
    HomePageComponent,
    BeerDetailsPageComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick({Home, UserPlus, Lightbulb, Gift, Trash2})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
