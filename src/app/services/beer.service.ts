import { Injectable } from '@angular/core';
import axios from 'axios';
import { Beer } from '@/models/beer.model';

// STEP 1 - Injectacle - créer dossier et class service
@Injectable({
  providedIn: 'root'
})
export class BeerService {
  public beers: Beer[] = [];

  constructor() { }

  public async addBeerFromApi() {
    // { data } - décompose la réponse de la promesse en récupérant juste la clé "data"
    const { data } = await axios.get("https://api.punkapi.com/v2/beers/random");
    const currentBeer = data[0];
    console.log(currentBeer);


    this.beers.push({
      id: currentBeer.id,
      name: currentBeer.name,
      tagline: currentBeer.tagline,
      description: currentBeer.description,
      alcoholLevel: currentBeer.abv,
      volValue: currentBeer.volume.value,
      volUnit: currentBeer.volume.unit,
      foodPairing: currentBeer.food_pairing,
      picturePath: currentBeer.image_url,
    });

    console.log(currentBeer);

  }

  // // STEP 3 Delete Beer in array - method witch delete one Beer in Beers array
  public removeBeerFromList(beer: Beer){
    this.beers = this.beers.filter((b) => b.id !== beer.id);
  }

  public displayBeerDetails(beer: Beer){
    this.beers = this.beers.filter((b) => b.id !== beer.id);
  }
}
