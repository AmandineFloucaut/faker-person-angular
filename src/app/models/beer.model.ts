export interface  Beer {
  id: number;
  name: string;
  tagline: string;
  description: number;
  alcoholLevel: number;
  volValue: number;
  volUnit: string;
  foodPairing: [];
  picturePath: string;
}