import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Beer } from '@/models/beer.model';

@Component({
  selector: 'app-beer',
  templateUrl: './beer.component.html',
  styleUrls: ['./beer.component.scss']
})
export class BeerComponent implements OnInit {

  @Input()
  public beer?: Beer;

  // STEP 1 Delete Beer in array
  @Output('deleted')
  public deletedEvent = new EventEmitter<Beer>();

  constructor() { }

  ngOnInit(): void {}

  get volume(){
    if(this.beer){
      return this.beer.volValue + " " + this.beer.volUnit;
    } else {
      return "Cet utilisateur n'existe pas";
    }

  }

  get foodsPairing(){
    let foods = "";
    if(this.beer){
      for(let food of this.beer.foodPairing){
         foods += food + ",  ";
      }
    }

   return foods;
  }

  // STEP 2 Delete Beer in array method witch send an event
  public clickOnDeletedBeer(){
    console.log("deleted");
    console.log(this.beer);
    this.deletedEvent.emit(this.beer);
  }

}
