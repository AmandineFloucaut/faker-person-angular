import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from '@/pages/home-page/home-page.component';
import { BeerDetailsPageComponent } from '@/pages/beer-details-page/beer-details-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'beers', pathMatch: 'full'},
  { path: 'beers', component: HomePageComponent},
  { path: 'beers/:id', component: BeerDetailsPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
